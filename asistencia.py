import read
import mysql
import led0
import led7
import subprocess
import RPi.GPIO as GPIO 
import sys
import time

accion_usuario = "entrada"

def send_to_module(tagId):
	"""Envia tag al modulo de kernel para que sea cargado a traves de la pagina"""
	f= open("/dev/loaduser","w")
	f.write("%s" % tagId)
	f.close()
	#f = open("/dev/loaduser","r",0)
	#rfid = f.read(10)
	#print "RFID: " + rfid
	#f.close()

def module_loaded(module_name):
    """Verifica si el modulo esta cargado, devuelve true o false"""
    lsmod_proc = subprocess.Popen(['lsmod'], stdout=subprocess.PIPE)
    grep_proc = subprocess.Popen(['grep', module_name], stdin=lsmod_proc.stdout)
    grep_proc.communicate()  # Block until finished
    return grep_proc.returncode == 0	

def pulsador(channel):  
	global accion_usuario
	if accion_usuario == "entrada":
		accion_usuario = "salida"
		print "Accion seleccionada: " + accion_usuario
	else:
		accion_usuario = "entrada"
		print "Accion seleccionada: " + accion_usuario
         

def main():
	#Configuro el pin 13 de la placa como un pulsador para que cambie la accion que se toma
	GPIO.setmode(GPIO.BOARD)  
	GPIO.setup(13, GPIO.IN, pull_up_down=GPIO.PUD_UP)
	GPIO.add_event_detect(13, GPIO.FALLING, callback=pulsador, bouncetime=300)
	
	while True:
	#Utilizo la funcion read del script read para obtener una lectura del RFID
		tagId = read.read()
		print (tagId)
	#Verifico si el modulo esta cargado
		loaded = module_loaded("loaduser") #cambiar encr si cambio el nombre del modulo
	#En el caso que el modulo este cargado, debe escribir el tag Id en el archivo del modulo
		if loaded == True:
			send_to_module(tagId)
			time.sleep(1)
	#De lo contrario, debe intentar cargar como lectura en la base de datos
		else:
			#action = "entrada" # cambiar accion para que tome una lectura de un boton/pulsador
			valor = mysql.insertReading(tagId,accion_usuario)
			if valor == True:
				led0.turnon()#prender LED verde
			else:
				led7.turnon()#prender LED rojo
	GPIO.cleanup()		

if __name__ == '__main__':
    try:
    	main()
    except KeyboardInterrupt:
    	GPIO.cleanup()
    	sys.exit(0)	

