@ To assemble, link, and run:
@
@  as -o blink.o blink.s 
@  gcc -o blink2 blink.o -lwiringPi 
@  sudo ./blink2 

@ ---------------------------------------
@	Data Section
@ ---------------------------------------
	
	 .data
	 .balign 4	
Intro: 	 .asciz  "Encendido de LED en GPIO 0\n"
ErrMsg:	 .asciz	"Abortando...\n"
pin:	 .int	0
delayMs: .int	1000
OUTPUT	 =	1
	
@ ---------------------------------------
@	Code Section
@ ---------------------------------------
	
	.text
	.global asm_main
	.extern printf
	.extern wiringPiSetup
	.extern delay
	.extern digitalWrite
	.extern pinMode
	
asm_main:   push 	{ip, lr}	@ push return address + dummy register
				@ for alignment

@  printf( "blink..." )	;
@ Muevo mensaje intro al registro r0, luego llamo a la funcion printf con bl
	ldr	r0, =Intro	@guardo lo que hay en la direccion Intro en el registro r0
        bl 	printf	@bl me likea con la funcion externa

@  if (wiringPiSetup() == -1) {
@     printf( "Setup didn't work... Aborting." ) ;
@     exit (1)					 ;
@  }
	bl	wiringPiSetup	@llamo a la funcion wiringPiSetup, configura el mapeo de puertos. si lo que devuelve(en r0) es -1 salgo. De lo contrario sigo
	mov	r1,#-1	        
	cmp	r0, r1          @en r0 esta lo que me devuelve wiringPi
	bne	init            @si son distintos, Z va a estar en 0 asi que salta
	ldr	r0, =ErrMsg
	bl	printf
	b	done

@  pinMode(pin, OUTPUT)		; 
init:
	ldr	r0, =pin 		@pongo en el registro r0 el numero de gpio que quiero configurar
	ldr	r0, [r0]		@luego, en r1 pongo la direccion que quiero (salida en este caso)
	mov	r1, #OUTPUT		
	bl	pinMode			@Finalmente llamo a pinmode para que configure el el pin
	
@	digitalWrite(pin, 1) ;
	ldr	r0, =pin  		@en esta porcion de codigo llamo a digitalWrite con el numero de pin y 1 para encenderlo
	ldr	r0, [r0]
	mov	r1, #1
	bl 	digitalWrite
	
@       delay(500)		 ;
	ldr	r0, =delayMs    @llamo a la funcion delay con el tiempo definido previamente en la seccion de datos
	ldr	r0, [r0]
	bl	delay

@       digitalWrite(pin, 0) 	;
	ldr	r0, =pin   		@apago el led
	ldr	r0, [r0]
	mov	r1, #0
	bl 	digitalWrite
	
done:	
        pop 	{ip, pc}	@ pop return address into pc