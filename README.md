# SisComp

Sistema asistencia de empleados:

El sistema trata sobre un control de asistencia y horarios de empleados mediante un lector RFID.
Cada empleado tiene una tarjeta y un horario correspondiente, con la cual puede ingresar acercando la misma al lector. Mediante un pulsador debe definir si esta entrando o saliendo.
El sistema niega la entrada si el usuario esta fuera de un margen de tiempo del horario de entrada salida.
En el caso de cualquier error se enciende un LED y de ser tomada la lectura correctamente se enciende otro.

La informacion referida a los usuarios, tarjetas y lecturas se encuentran en una base de datos MySQL, la cual esta en la misma placa. Ademas, contamos con una pagina web mediante la cual se puede hacer consultas, agregar usuarios y agregar horarios para cada usuario.

Enlace al informe del trabajo:
https://docs.google.com/document/d/18ZV4jUWzwluCKf0HxLEHkhclUSvQ_lIIeUCpS0OH0zU/edit?usp=sharing