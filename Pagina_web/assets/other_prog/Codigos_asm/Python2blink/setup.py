from distutils.core import setup, Extension
setup(name='led7', version='1.0',  \
      ext_modules=[Extension('led7', ['main.c',], libraries=['wiringPi'], include_dirs=['/usr/lib'], extra_objects=["led.o"])])