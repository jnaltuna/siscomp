#include <Python.h>
#include <wiringPi.h>
#include <stdio.h>
#include <stdlib.h>
#include "cdecl.h"

//int PRE_CDECL asm_main( void ) POST_CDECL;

static PyObject* turnon(PyObject* self) {

  int ret_status;
    ret_status = asm_main();
	
  return Py_BuildValue("s", "Codigo asm!!");
}

static char led0_docs[] =
   "led0( ): Any message you want to put here!\n";

static PyMethodDef led0_funcs[] = {
   {"turnon", (PyCFunction)turnon, 
      METH_NOARGS, led0_docs},
      {NULL}
};

void initled0(void) {
   Py_InitModule3("led0", led0_funcs,
                  "Extension module example!");
}