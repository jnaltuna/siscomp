from distutils.core import setup, Extension
setup(name='led0', version='1.0',  \
      ext_modules=[Extension('led0', ['main.c',], libraries=['wiringPi'], include_dirs=['/usr/lib'], extra_objects=["led.o"])])