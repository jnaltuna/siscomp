# SisComp

Sistema asistencia de empleados:

El sistema consiste en un control de asistencia y horarios mediante el uso de RFID. Cada empleado cuenta con una tarjeta con la cual debe ingresar y retirarse.

Cada tarjeta va a tener asociado un nombre, DNI y horarios de entrada/salida.
El empleado va a ingresar al sistema en su horario correspondiente, con un determinado margen de tiempo. Si esta fuera de ese margen, se le permite entrar igual pero se le notifica mediante un LED o una pantalla LCD.
Ese horario se va a poder modifciar por el administrador.
Ademas, el administrador y posiblemente el empleado pueden consultar mediante una pagina web los ultimos ingresos.

El enlace de la cual sacamos la idea es el siguiente:

https://hackaday.io/project/164187-raspberry-pi-attendance-system