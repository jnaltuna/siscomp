#include <linux/init.h>           
#include <linux/module.h>         
#include <linux/device.h>         
#include <linux/kernel.h>        
#include <linux/fs.h>             
#include <linux/uaccess.h>          
#define  DEVICE_NAME "loaduser"   
#define  CLASS_NAME  "SO1"    
 
MODULE_LICENSE("GPL");           
MODULE_AUTHOR("Julian Altuna-Nicolas Goldman");    
MODULE_DESCRIPTION("Device driver para sistcomp"); 
MODULE_VERSION("1.0"); 

static int    majorNumber;                  //Almacena el numero de dispositivo. Determina que driver maneja el archivo.
static char   message[256] = {0};           //Arreglo en el que se almacena 
static short  longmsg;                     //Tamaño del mensaje
static int    numberOpens = 0;              //Numero de veces que se abre el archivo
static struct class*  encrcharClass  = NULL; ///< The device-driver class struct pointer
static struct device* encrcharDevice = NULL; ///< The device-driver device struct pointer
 
// Prototipos de funciones que voy a utilizar
static int     dev_open(struct inode *, struct file *);
static int     dev_release(struct inode *, struct file *);
static ssize_t dev_read(struct file *, char *, size_t, loff_t *);
static ssize_t dev_write(struct file *, const char *, size_t, loff_t *);
 
/* 
 * Establezco mis funciones como las que voy a usar para manipular el archivo de driver
 */
static struct file_operations fops =
{
   .open = dev_open,
   .read = dev_read,
   .write = dev_write,
   .release = dev_release,
};

static char *mydevnode(struct device *dev, umode_t *mode)
{
     if (mode)
         *mode = 0666; /* or whatever permissions you want */
     return NULL; /* could override /dev name here too */
}
 
/** Funcion de inicializacion
 *  __init hace que se descarte de memoria la funcion una vez que fue ejecutado
 *  Registro el disposiitivo (asigno numero mayor)
 *  
 */
static int __init loaduser_init(void){
   printk(KERN_INFO "Inicializando el modulo\n");
 
   majorNumber = register_chrdev(0, DEVICE_NAME, &fops);
   if (majorNumber<0){
      printk(KERN_ALERT "Fallo el registro\n");
      return majorNumber;
   }
   printk(KERN_INFO "Registro exitoso %d\n", majorNumber);
 
   // Registro clase
   encrcharClass = class_create(THIS_MODULE, CLASS_NAME);
   if (IS_ERR(encrcharClass)){                 // Verifico errores. Finalizo si hay
      unregister_chrdev(majorNumber, DEVICE_NAME);
      printk(KERN_ALERT "Failed to register device class\n");
      return PTR_ERR(encrcharClass);          // Correct way to return an error on a pointer
   }
   encrcharClass->devnode=mydevnode;
   printk(KERN_INFO "Se registro correctamente la clase\n");
 
   // Register the device driver
   encrcharDevice = device_create(encrcharClass, NULL, MKDEV(majorNumber, 0), NULL, DEVICE_NAME);
   if (IS_ERR(encrcharDevice)){                // Verifico errores. Finalizo si hay
      class_destroy(encrcharClass);           
      unregister_chrdev(majorNumber, DEVICE_NAME);
      printk(KERN_ALERT "Failed to create the device\n");
      return PTR_ERR(encrcharDevice);
   }
   printk(KERN_INFO "Se registro el disposiitivo correctamente\n");
   return 0;
}
 
/*
 * Funcion de salida. Borro todos los registros realizados en la inicializacion
 */
static void __exit loaduser_exit(void){
   device_destroy(encrcharClass, MKDEV(majorNumber, 0));    
   class_unregister(encrcharClass);                          
   class_destroy(encrcharClass);                             
   unregister_chrdev(majorNumber, DEVICE_NAME);             
   printk(KERN_INFO "Finalizando modulo\n");
}
 
/** Esta funcion es llamada cada vez que se abre el archivo. Aumento un contador para llevar un registro de las veces que se abrio
 *  @param inodep A pointer to an inode object (defined in linux/fs.h)
 *  @param filep A pointer to a file object (defined in linux/fs.h)
 */
static int dev_open(struct inode *inodep, struct file *filep){
   numberOpens++;
   printk(KERN_INFO "Mod: Dis fue abierto %d veces\n", numberOpens);
   return 0;
}
 
/**
 * Esta funcion se llama cada vez que el dispositivo es leido desde el espacio de usuario. 
   Info es enviada desde el dispositivo al usuario. Uso la funcion copy_to_user() para enviar
   buffer al usuario.
 *  @param filep A pointer to a file object (defined in linux/fs.h)
 *  @param buffer The pointer to the buffer to which this function writes the data
 *  @param len The length of the b
 *  @param offset The offset if required
 */
static ssize_t dev_read(struct file *filep, char *buffer, size_t len, loff_t *offset){
   int error_count = 0;
   // copy_to_user has the format ( * to, *from, size) and returns 0 on success
   error_count = copy_to_user(buffer, message, longmsg);
 
   if (error_count==0){            // if true then have success
      printk(KERN_INFO "loaduser: Se enviaron %d caracteres\n", longmsg);
      return (longmsg=0);  // clear the position to the start and return 0
   }
   else {
      printk(KERN_INFO "Fallo el envio\n");
      return -EFAULT;             //Devuelvo bad address message, -14
   }
}
 
/** 
 * Esta funcion se llama cada vez que el dispositivo es escrito desde el espacio de usuario.
   Info es enviada desde el espacio de usuario al dispositivo. Se almacena en el arreglo, 
   sumandole '10' para encriptar la cadena.
 *  @param filep A pointer to a file object
 *  @param buffer The buffer to that contains the string to write to the device
 *  @param len The length of the array of data that is being passed in the const char buffer
 *  @param offset The offset if required
 */
static ssize_t dev_write(struct file *filep, const char *buffer, size_t len, loff_t *offset){

   sprintf(message, "%s", buffer);   
   longmsg  = strlen(message);                 
   
   //for(i=0 ; i < 256;i++){
   //	message[i]  = message[i] + 10;
   //s}

   printk(KERN_INFO "loaduser: Recibi %zu caracteres\n", len);
   return len;
}
 
/** Es llamada cada vez que se cierra el archivo desde el espacio de usuario
 *  @param inodep A pointer to an inode object (defined in linux/fs.h)
 *  @param filep A pointer to a file object (defined in linux/fs.h)
 */
static int dev_release(struct inode *inodep, struct file *filep){
   printk(KERN_INFO "loaduser: Dispositivo cerrado\n");
   return 0;
}
 
/** @brief A module must use the module_init() module_exit() macros from linux/init.h, which
 *  identify the initialization function at insertion time and the cleanup function (as
 *  listed above)
 */
module_init(loaduser_init);
module_exit(loaduser_exit);
