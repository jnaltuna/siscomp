from time import strftime,localtime
import datetime
import MySQLdb
#from unidecode import unidecode

def connect():
	# Mysql connection setup. Insert your values here
	return MySQLdb.connect(host="localhost", user="pi", passwd="rasp2019", db="sistcomp")

def insertReading(tagId,action):
	db = connect()
	cur = db.cursor()

	#Verificacion que el tagId leido sea de un usuario valido
	query = """SELECT DNI_usuario FROM tarjetas where tagid = (%s)"""
	cur.execute(query,(tagId,))
	row = cur.fetchone();
	
	if(row == None):
		print ("No hay usuario con ese tag")
		return False
	#else:
	#	print ("Usuario encontrado")
	
	#Verificacion que la ultima accion no sea igual a la actual
	query = """SELECT accion from lecturas where tagid_leido = (%s) order by timestamp desc limit 1"""
	cur.execute(query,(tagId,))
	row = cur.fetchone();
	#revisar logica!!!!!!!!!!!!!
	if (row != None):
		if action in row:
			print("Es la misma accion")
			return False
	#else:
	#	print("No es lo mismo")

	#Verificar horario de entrada salida
	#Dependiendo de la accion recibida tomo el valor correspondiente de la base de datos
	if action == "entrada":
		print ("entrada")
		query=""" SELECT hora_entrada from horarios where DNI = (select DNI from tarjetas where tagid = (%s) limit 1) order by fecha_horario desc limit 1"""
	else:	
		print ("salida")
		query=""" SELECT hora_salida from horarios where DNI = (select DNI from tarjetas where tagid = (%s) limit 1) order by fecha_horario desc limit 1"""
	cur.execute(query,(tagId,))
	row = cur.fetchone();
	print(row[0])
	#Defino el rango de tiempo en el cual puede entrar el empleado
	tiempo_min = row[0]-datetime.timedelta(minutes = 3000)
	tiempo_max = row[0]+datetime.timedelta(minutes = 3000)

	#Tomo horas y minutos actuales
	time = datetime.datetime.now()
	curr_time = datetime.timedelta(hours = time.hour,minutes = time.minute)

	#Comparo valor tomado de tabla con el tiempo actual, si esta dentro del rango lo agrego a la DB
	if curr_time >= tiempo_min:
		if curr_time <= tiempo_max:
			print("Tiempo valido")
			query= """INSERT INTO lecturas (idlecturas, tagid_leido, timestamp,accion) VALUES (0, %s, %s, %s)"""
			currentTime=strftime("%Y%m%d%H%M%S", localtime())
			cur.execute(query,(tagId,currentTime,action))
			db.commit()
		else:
			print("Mayor que el max")
			return False
	else:
		print("Menor que el min")
		return False
					
	db.close()
	return True