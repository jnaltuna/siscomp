#include <Python.h>
#include <wiringPi.h>
#include <stdio.h>
#include <stdlib.h>
#include<fcntl.h>

//int PRE_CDECL asm_main( void ) POST_CDECL;
#define BUFFER_LENGTH 80              //Longitud del buffer
static char encrypted[BUFFER_LENGTH];     //Buffer con datos encriptados

static PyObject* rfid(PyObject* self) {

  int ret, fd;
  fd = open("/dev/loaduser",O_RDWR);
  if (fd < 0){
      perror("Fallo la apertura del archivo...");
      return errno;
  }

  ret = read(fd, encrypted, BUFFER_LENGTH);        // Leo mensaje encriptado
  if (ret < 0){
     perror("Fallo la lectura.");
     return errno;
  }
  close(fd);

	
  return Py_BuildValue("s", encrypted);
}

static char rfid_docs[] =
   "led0( ): Any message you want to put here!\n";

static PyMethodDef rfid_funcs[] = {
   {"rfid", (PyCFunction)rfid, 
      METH_NOARGS, rfid_docs},
      {NULL}
};

void initrfid(void) {
   Py_InitModule3("rfid", rfid_funcs,
                  "Extension module example!");
}