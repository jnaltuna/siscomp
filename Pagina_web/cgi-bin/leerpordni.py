#!/usr/bin/python


# Import modules for CGI handling 
import cgi, cgitb, MySQLdb

def connect():
	# Mysql connection setup. Insert your values here
	return MySQLdb.connect(host="localhost", user="pi", passwd="rasp2019", db="sistcomp")    

# Create instance of FieldStorage 
form = cgi.FieldStorage() 

# Get data from fields
dni = form.getvalue('dni')

print "Content-type:text/html\r\n\r\n"
print "<html>"
print "<head>"
print "<style> table, th, td { border: 1px solid black; text-align: center; } </style>"
print "</head>"
print "<body>"

#me conecto a la base datos
db = connect()
cur = db.cursor()
query = """SELECT timestamp,accion from lecturas where tagid_leido = (select tagid from tarjetas where DNI_usuario = (%s) order by fecha_habilitacion desc limit 1) order by timestamp desc """
cur.execute(query,(dni,))
print "<table> <tr> <th>Timestamp</th><th>Accion</th></tr>"
while True:
	row=cur.fetchone()
	if row is None:
		break
	print "<tr>"
	for i in range(len(row)):
		print "<td>",row[i],"</td>"
	print "</tr>"

db.close()

print "</body>"
print "</html>"

