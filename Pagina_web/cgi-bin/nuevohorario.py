#!/usr/bin/python


# Import modules for CGI handling 
import cgi, cgitb, MySQLdb, rfid, datetime

def connect():
	# Mysql connection setup. Insert your values here
	return MySQLdb.connect(host="localhost", user="pi", passwd="rasp2019", db="sistcomp")    

# Create instance of FieldStorage 
form = cgi.FieldStorage() 

# Get data from fields
dni 		  = form.getvalue('dni')
hora_entrada  = form.getvalue('hora_entrada')
hora_salida   = form.getvalue('hora_salida')


print "Content-type:text/html\r\n\r\n"
print "<html>"
print "<head>"
print "</head>"
print "<body>"

try:
#me conecto a la base datos
	db = connect()
#print "me conecte"
	cur = db.cursor()
#print "creo el cursor"
	query= """INSERT INTO horarios (DNI, fecha_horario, hora_entrada, hora_salida) VALUES (%s, %s, %s, %s)"""
	now = datetime.datetime.now()
	str_now = now.date().isoformat()
	#print "aca"
	ent = datetime.timedelta(hours = int(hora_entrada))
	sal = datetime.timedelta(hours = int(hora_salida))
	#print "aca1	"
	#print (ent)
	#print (sal)
	cur.execute(query,(dni,str_now,ent,sal))
	#print "hice esto"
	db.commit()

	print "<p> Horario modificado exitosamente </p>"
	db.close()
except :
	print "Error al cargar el horario."




print "<p> Redirigiendo en 5 segundos... </p>"
print "<html> <head> <meta http-equiv=\"REFRESH\" content=\"5;url=./../modificar_horario.html\"> </head> </html>"

print "</body>"
print "</html>"

