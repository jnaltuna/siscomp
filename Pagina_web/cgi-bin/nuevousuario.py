#!/usr/bin/python


# Import modules for CGI handling 
import cgi, cgitb, subprocess, MySQLdb, rfid, datetime, os

def module_loaded(module_name):
    """Verifica si el modulo esta cargado, devuelve true o false"""
    lsmod_proc = subprocess.Popen(['lsmod'], stdout=subprocess.PIPE)
    FNULL = open(os.devnull, 'w')
    grep_proc = subprocess.Popen(['grep', module_name], stdin=lsmod_proc.stdout, stdout=FNULL, stderr=FNULL)
    grep_proc.communicate()  # Block until finished
    return grep_proc.returncode == 0

def connect():
	# Mysql connection setup. Insert your values here
	return MySQLdb.connect(host="localhost", user="pi", passwd="rasp2019", db="sistcomp")    

# Create instance of FieldStorage 

print "Content-type:text/html\r\n\r\n"
print "<html>"
print "<head>"
print "</head>"
print "<body>"

form = cgi.FieldStorage() 

# Get data from fields
first_name = form.getvalue('name')
last_name  = form.getvalue('surname')
dni		   = form.getvalue('dni')
domicilio  = form.getvalue('domicilio')
#email	   = form.getvalue('email')



loaded = module_loaded("loaduser")

if loaded == True:
	#hacer todo el desarrollo aca
	#f = open("/dev/loaduser" , "rw")
	#rfid = f.read(10)
	#print (f.read(10))
	#f.close()
	#print "RFID: " + rfid
	rfid = rfid.rfid()
	#print "RFID: " + rfid
	try :
		#me conecto a la base datos
		db = connect()
		cur = db.cursor()
		query= """INSERT INTO usuarios (DNI, Nombre, Apellido, Domicilio, Area, Activo) VALUES (%s, %s, %s, %s, "Direccion", 1)"""
		cur.execute(query,(dni,first_name,last_name,domicilio))
		now = datetime.datetime.now()
		str_now = now.date().isoformat()
		query= """INSERT INTO tarjetas (tagid, DNI_usuario, fecha_habilitacion) VALUES (%s, %s, %s)"""
		cur.execute(query,(rfid,dni,str_now))
		db.commit()
		print "<p> Usuario cargado exitosamente </p>"
		db.close()
		os.system("sudo rmmod loaduser")

	except :
		print "Error al cargar el usuario."	

else:
	print "Error - modulo no cargado"
	#imprimir algun error y volver
print "<p> Redirigiendo en 5 segundos... </p>"
print "<html> <head> <meta http-equiv=\"REFRESH\" content=\"5;url=./../cargar_nuevo.html\"> </head> </html>"

print "</body>"
print "</html>"

